exports.info = {
  title: "璞真之道",
  description: "中山北路最高仰望，61-89坪樹海名宅。星級飯店為鄰，藝術精品相伴，國門特區前瞻國際。在百年林蔭之上，寫下層峰的未來視野",
  ogTitle: "璞真之道｜中山北路第一排，23層景觀地標",
  ogDescription: "中山北路最高仰望，61-89坪樹海名宅。星級飯店為鄰，藝術精品相伴，國門特區前瞻國際。在百年林蔭之上，寫下層峰的未來視野",
  ogImage: "https://case.hiyes.tw/pjavenue/img/og.jpg",
  keywords: "璞真之道"
};
